FROM ubuntu

RUN apt update
RUN apt install -y software-properties-common
RUN add-apt-repository -y ppa:jonathonf/ffmpeg-4
RUN apt install -y ffmpeg
RUN apt install -y curl

RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
RUN apt  install -y nodejs
RUN apt  install -y awscli